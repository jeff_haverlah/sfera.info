# coding: utf-8

import netifaces
import nmap
import os
import requests

from config import DEVICES
from requests.exceptions import ConnectionError, ReadTimeout

def run():
    netinfo = check_network()
    network = None
    if os.path.exists("scanned_hosts.txt"):
        os.remove("scanned_hosts.txt")
    for info in netinfo:
        ip, mask = info
        network = "/".join(info)
        nmap_scan(network)

def nmap_scan(network):
    print "Using network: " + network
    nm = nmap.PortScanner()
    args = "-n -sP -T4 -PA80"
    nm.scan(hosts=network, arguments=args)
    for host in nm.all_hosts():
        if ping_host(host):
            with open("scanned_hosts.txt", "a") as f:
                f.write(host)
                f.write("\n")


def check_network():
    ifaces = netifaces.interfaces()
    networks = []
    for iface in ifaces:
        if not iface.startswith(("lo", "docker", "wlan")):
            network = netifaces.ifaddresses(iface)
            nets = [(net["addr"], mask_numerical(net["netmask"])) for net in network[netifaces.AF_INET]]
            networks += nets

    return networks

def mask_numerical(mask):
    segs = mask.split(".")
    mask = 0
    adds = (0, 128, 192, 224, 240, 248, 252, 254, 255)
    for i in segs:
        for j in range(0, len(adds)):
            if int(i) == adds[j]:
                mask += j
    return str(mask)

def ping_host(host):
    url = "http://{}/common/info.cgi".format(host)
    try:
        r = requests.get(url, timeout=10)
        response = r.text.split("\r\n")
        device = None
        version = None
        for item in response:
            if item.startswith("model"):
                device = item.split("=")[1]
            if item.startswith("version"):
                version = item.split("=")[1]
        if (device in DEVICES) and need_upgrade(device, version):
            print "Device: {}, host: {}".format(device, host)
            return True
    except (ConnectionError, ReadTimeout):
        return False
    return False

def need_upgrade(device, version):
    if version:
        if version.startswith("v"):
            current_firmware = version[1:].split(".")
        else:
            current_firmware = version.split(".")
        stable_firmware = DEVICES[device]["firmware"].split(".")
        if int(current_firmware[0]) < int(stable_firmware[0]):
            return True
        if int(current_firmware[1]) < int(stable_firmware[1]):
            return True
    return False

if __name__ == "__main__":
    run()
