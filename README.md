# README #

## Инструкция по установке ##

__версия Python:__

* Python 2.7.3

__Используемые пакеты:__

* argparse==1.2.1
* click==6.6
* netifaces==0.10.4
* python-nmap==0.6.0
* requests==2.9.1
* wsgiref==0.1.2

Для начала склонировать проект с git'а:
```bash
git clone git@bitbucket.org:jeff_haverlah/sfera.info.git
```

Установить виртуальное окружение в папке с проектом и активировать его.
Установить утилиту nmap:
```bash
cd sfera.info
sudo apt-get install python-virtualenv nmap
virtualenv .venv
source .venv/bin/activate
```

Установить пакеты через `pip install`, либо любым другим удобным способом.
Пример установки:
```bash
pip install click netifaces python-nmap requests
```

Однако, можно использовать файл requirements.txt и установить все зависимости
из него следующим образом:
```bash
pip install -r requirements.txt
```

На этом формирование виртуального окружения закончено, можно запускать cli.py.
Он просканирует интерфейсы eth на наличие устройств DCS-3010 и DCS-2210,
занесет их айпишники в scanned_hosts.txt, после чего обновит у всех прошивку на
актуальную v1.20.


## Техническая информация ##
### D-link ftp ###

Надо искать по номеру модели. В данном случае это модели 2210 и 3010.
Соответственно url будет следующим http://ftp.dlink.ru/pub/Multimedia/2210 или
http://ftp.dlink.ru/pub/Multimedia/3010. Там надо искать последнюю прошивку с
раширением .bin. Формат файла примерно такой - DCS-2210_FW_v1.xx.xx.bin.

### Работа с камерой ###

1. Перепрошивка - на урл /config/firmwareupgrade.cgi отправить файл .bin с
   прошивкой.
2. Перезагрузка - вызвать урл /config/system_reboot.cgi.
3. Экспорт настроек - вызвать урл /setup/Settings.CFG
4. Импорт настроек - на урл /config/upload_settings.cgi отправить файл c
   настройкой.
5. Настройка сети - на урл /config/network.cgi POST-запросом отправить
   параметры сети


