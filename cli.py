# coding: utf-8

import scan
import upgrade

def cli():
    scan.run()
    upgrade.run()

if __name__ == "__main__":
    cli()
