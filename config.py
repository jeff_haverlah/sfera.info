# coding: utf-8

DEVICES = {
    "DCS-2210": {
        "firmware": "1.24",
        "save_config_url": "cgi-bin/exportconf.cgi",
        "load_config_url": "cgi-bin/loadconf.cgi",
        "upgrade_firmware_url": "update.cgi",
        "firmware_file_alias": "upfile",
        "reboot_url": "cgi-bin/zbcreboot.cgi?mode=0"
    },
    "DCS-3010": {
        "firmware": "1.12",
        "save_config_url": "setup/Settings.CFG",
        "load_config_url": "config/upload_setting.cgi",
        "upgrade_firmware_url": "config/firmwareupgrade.cgi",
        "firmware_file_alias": "fimage",
        "reboot_url": "config/system_reboot.cgi"
    }
}

DEFAULT_IP = "192.168.0.20"

DEFAULT_CREDENTIALS = (u"admin", u"")
