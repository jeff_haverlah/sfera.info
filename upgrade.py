# coding: utf-8

import click
import json
import requests
import time

from glob import glob
from multiprocessing import Process
from requests.exceptions import ConnectionError, ReadTimeout

from config import DEFAULT_CREDENTIALS, DEFAULT_IP, DEVICES


NETWORK_SETTINGS = {}

@click.command()
@click.option("--reboot/--no-reboot", default=False, is_flag=True,
        help=u"Указывает, что после установки прошивок камеры перезагружать"+
             u" не требуется. По умолчанию передается --no-reboot.")
@click.option("--config/--no-config", default=False, is_flag=True,
        help=u"Флаг отвечает за использование настроек по умолчанию,"+
             u" находящихся в configs/default.cfg")
@click.option("--hosts", default="scanned_hosts.txt",
        help=u"Указывает путь к файлу с ip адресами камер. По умолчанию"+
             u" используется --no-config и файл 'scanned_hosts.txt',"+
             u" формируемый сканером сети")
@click.option("--devices", default="DCS-3010,DCS-2210",
        help=u"Можно указать список устройств, которые будут обновлять"+
             u" прошивку. Имена устройств, указанные в этом параметре будут"+
             u" сравниваться с заголовком ответа www-authenticate. По"+
             u" умолчанию используются устройства D-link DCS-3010 и DCS-2210"+
             u" Передавать через запятую в формате `DCS-3010,DCS-2210`")
def run(reboot, config, hosts, devices):
    """Upgrading firmware"""
    hosts_info = _parse_hosts(hosts)
    for ip, auth in hosts_info.items():
        session = _auth(ip, auth)
        if session:
            device = _check_device(ip, DEVICES.keys())
            if not config:
                _download_config(session, ip, device)
            terminated = _process(_upload_firmware, (session, ip, device), 300)
            if not terminated:
                _set_ip(ip)
            _process(_upload_config, (session, ip, device, config), 100)
            if reboot:
                _reboot(session, ip, device)
        else:
            print "Not authentificate!"

def _download_config(session, ip, device):
    print "Downloading config ..."
    save_config_url = DEVICES[device]["save_config_url"]
    url = "http://{}/{}".format(ip, save_config_url)
    r = session.get(url, stream=True)
    with open("configs/{}.cfg".format(ip), "w") as f:
        f.write(r.raw.read())
    print r.ok

def _upload_firmware(session, ip, device):
    print "Upgrading firmware ..."
    upgrade_firmware_url = DEVICES[device]["upgrade_firmware_url"]
    stable_firmware = DEVICES[device]["firmware"]
    firmware_file_alias = DEVICES[device]["firmware_file_alias"]
    _get_network_info(ip)
    path = glob("firmware/*{}*{}*.bin".format(device, stable_firmware))[0]
    fn = path.split("/")[-1]
    firmware = open(path, "rb")
    header = "application/octet-stream"
    url = "http://{}/{}".format(ip, upgrade_firmware_url)
    r = session.post(url, files={firmware_file_alias: (fn, firmware, header)})
    print r.ok

def _upload_config(session, ip, device, conf=None):
    print "Uploading old config ..."
    path = conf if conf else "configs/{}.cfg".format(ip)
    config = _get_config(path)
    load_config_url = DEVICES[device]["load_config_url"]
    url = "http://{}/{}".format(ip, load_config_url)
    try:
        r = session.get(url, data=config, stream=True)
        print r.ok
    except ConnectionError:
        time.sleep(100)

def _reboot(session, ip, device):
    print "Rebooting camera ..."
    reboot_url = DEVICES[device]["reboot_url"]
    url = "http://{}/{}".format(ip, reboot_url)
    r = session.get(url)
    print r.ok

def _process(target, args, duration):
    p = Process(target=target, args=args)
    p.start()
    p.join(duration)
    if p.is_alive():
        p.terminate()
        p.join()
        return True
    return False

def _set_ip(ip):
    print "Setting up network settings ..."
    session = _auth(DEFAULT_IP, DEFAULT_CREDENTIALS)
    if session:
        subnet_mask = NETWORK_SETTINGS.get("netmask", "255.255.255.0")
        default_router = NETWORK_SETTINGS.get("gateway", "0.0.0.0")
        primary_dns = "8.8.8.8"
        secondary_dns = "8.8.4.4"
        url = "http://192.168.0.20/config/network.cgi"
        params = {
            "dhcp": "off",
            "pppoe": "off",
            "pppoepass": "",
            "pppoeuser": "",
            "upnp": "on",
            "webshow": "on",
            "ip": ip,
            "netmask": subnet_mask,
            "gateway": default_router,
            "dns1": primary_dns,
            "dns2": secondary_dns
            }
        r = session.post(url, data=params)
        time.sleep(60)
        print r.ok

def _auth(ip, credentials):
    url = "http://{}".format(ip)
    s = requests.Session()
    s.auth = credentials
    return s if s.get(url) else None

def _parse_hosts(fn):
    if fn == "scanned_hosts.txt":
        with open(fn, "r") as f:
            hosts = f.readlines()
        result = {}
        for host in hosts:
            if host != "":
                result[host[:-1]] = DEFAULT_CREDENTIALS
        return result
    else:
        return json.load(fn)

def _check_device(ip, devices):
    url = "http://{}/common/info.cgi".format(ip)
    try:
        r = requests.get(url, timeout=10)
        response = r.text.split("\r\n")
        device = None
        for item in response:
            if item.startswith("model"):
                device = _get_value(item)
        if device in devices:
            return device
    except (ConnectionError, ReadTimeout):
        return None
    return None

def _get_network_info(host):
    url = "http://{}/common/info.cgi".format(host)
    try:
        r = requests.get(url, timeout=10)
        response = r.text.split("\r\n")
        for item in response:
            if item.startswith("netmask"):
                NETWORK_SETTINGS["netmask"] = _get_value(item)
            if item.startswith("gateway"):
                NETWORK_SETTINGS["gateway"] = _get_value(item)
    except (ConnectionError, ReadTimeout):
        print "No network settings. Host is down."

def _get_value(item):
    return item.split("=")[1]

def _get_config(path):
    with open(path, "rb") as f:
        return f.read()


if __name__ == "__main__":
    run()
